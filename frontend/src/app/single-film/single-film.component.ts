import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Film } from '../models/Film.model';
import { FilmsService } from '../service/films.service'
import { Router, Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-film',
  templateUrl: './single-film.component.html',
  styleUrls: ['./single-film.component.css']
})
export class SingleFilmComponent implements OnInit {

  public film: Film;
  public loading: boolean;

  constructor(private filmService: FilmsService,
              private router: Router,
              private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.loading = true;
    this.route.params.subscribe(
      (params: Params) => {
        this.filmService.getFilmById(params.id).then(
          (film: Film) => {
            this.loading = false;
            this.film = film;;
          }
        );
      }
    );
  }

  onGoBack() {
    this.router.navigate(['']);
  }

}

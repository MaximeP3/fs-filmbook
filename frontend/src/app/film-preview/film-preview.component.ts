import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Film } from '../models/Film.model';
import { FilmsService } from '../service/films.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-film-preview',
  templateUrl: './film-preview.component.html',
  styleUrls: ['./film-preview.component.css']
})
export class FilmPreviewComponent implements OnInit {

  public films: Film[] = [];
  public loading: boolean;

  private filmsSub: Subscription;

  constructor(private filmService: FilmsService,
              private router: Router) { }

  ngOnInit(): void {
    this.loading = true;
    this.filmsSub = this.filmService.films$.subscribe(
      (films) => {
        this.films = films;
        this.loading = false;
      }
    );
    this.filmService.getFilms();
  }

  onFilmClicked(id: string) {
    this.router.navigate(['/film/' + id]);
  }

}

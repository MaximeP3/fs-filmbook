export class Film {
  constructor(
    public _id: string,
    public title: string,
    public description: string,
    public imageUrl: string,
    public grade: number,
    public nbVote: number,
    public release: string
  ) {}
}

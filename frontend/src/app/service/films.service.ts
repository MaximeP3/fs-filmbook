import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Film } from '../models/Film.model';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {


  constructor(private http: HttpClient) { }

  private films: Film[];
  public films$ = new Subject<Film[]>();

  getFilms() {
    this.http.get('http://localhost:3000/api/films').subscribe(
        (films: Film[]) => {
          if (films) {
            this.films = films['films'];
            this.emitFilms();
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  emitFilms() {
    this.films$.next(this.films);
  }

  getFilmById(id: string) {
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:3000/api/films/' + id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

}

const express = require('express')
const router = express.Router()

const filmsCtrl = require('../controllers/films')

router.get('/', filmsCtrl.getAllFilms)
router.get('/:id', filmsCtrl.getOneFilm)

module.exports = router

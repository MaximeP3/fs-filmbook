const Film = require('../models/Film')

exports.getAllFilms = (req, res, next) => {
  Film.find()
    .then(films => res.status(200).json({ films }))
    .catch(error => res.status(400).json({ error }))
}

exports.getOneFilm = (req, res, next) => {
  Film.findOne({ _id: req.params.id })
    .then(film => res.status(200).json(film))
    .catch(error => res.status(404).json({ error }))
}

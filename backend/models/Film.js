const mongoose = require('mongoose')

const filmSchema = mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  imageUrl: { type: String, required: true },
  grade: { type: Number, required: true },
  nbVote: { type: Number, required: true },
  release: { type: String, required: true }
})

module.exports = mongoose.model('Film', filmSchema)
